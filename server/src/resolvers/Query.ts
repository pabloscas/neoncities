import { getUserId, Context } from '../utils'

export const Query = {
  feed(parent, args, ctx: Context, info) {
    return ctx.db.query.posts({ where: { isPublished: true } }, info)
  },

  drafts(parent, args, ctx: Context, info) {
    const id = getUserId(ctx)

    const where = {
      isPublished: false,
      author: {
        id
      }
    }

    return ctx.db.query.posts({ where }, info)
  },

  post(parent, { id }, ctx: Context, info) {
    return ctx.db.query.post({ where: { id } }, info)
  },

  me(parent, args, ctx: Context, info) {
    const id = getUserId(ctx)
    return ctx.db.query.user({ where: { id } }, info)
  },

  countries(parent, args, ctx: Context, info) {
    return ctx.db.query.countries({}, info)
  },

  country(parent, { id }, ctx: Context, info) {
    return ctx.db.query.country({ where: { id } }, info)
  },

  cities(parent, args, ctx: Context, info) {
    return ctx.db.query.cities({}, info)
  },

  city(parent, { id }, ctx: Context, info) {
    return ctx.db.query.city({ where: { id } }, info)
  },

  pictures(parent, args, ctx: Context, info) {
    return ctx.db.query.pictures({}, info)
  },

  picture(parent, { id }, ctx: Context, info) {
    return ctx.db.query.picture({ where: { id } }, info)
  },

  authors(parent, args, ctx: Context, info) {
    return ctx.db.query.authors({}, info)
  },

  author(parent, { id }, ctx: Context, info) {
    return ctx.db.query.author({ where: { id } }, info)
  },
}
