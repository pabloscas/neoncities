import { Context } from '../../utils'

export const author = {
  async createAuthor(parent, { name, instagram, website }, ctx: Context, info) {
    return ctx.db.mutation.createAuthor(
      {
        data: {
            name,
            instagram,
            website
        }
      },
      info
    )
  },

  async deleteAuthor(parent, { id }, ctx: Context, info) {
    const authorExists = await ctx.db.exists.Author({
      id
    })
    if (!authorExists) {
      throw new Error(`Author not found`)
    }

    return ctx.db.mutation.deleteAuthor({ where: { id } })
  },
}
