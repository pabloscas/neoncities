import { Context } from '../../utils'

export const picture = {
  async createPicture(parent, { title, authorId, cityId, url }, ctx: Context, info) {
    // check if Author exists, otherwise throw error
    const authorExists = await ctx.db.exists.Author({
        id: authorId
    })
    if (!authorExists) {
        throw new Error(`Author not found`)
    }
    // check if Author exists, otherwise throw error
    const cityExists = await ctx.db.exists.City({
        id: cityId
    })
    if (!cityExists) {
        throw new Error(`City not found`)
    }
    return ctx.db.mutation.createPicture(
      {
        data: {
            title,
            url,
            city: {
                connect: { id: cityId },
            },
            author: {
                connect: { id: authorId },
            },
        }
      },
      info
    )
  },

  async updatePicture(parent, { id, title, authorId, cityId, url }, ctx: Context, info) {
    const pictureExists = await ctx.db.exists.Picture({
      id
    })
    if (!pictureExists) {
      throw new Error(`Picture not found`)
    }

    return ctx.db.mutation.updatePicture({
      where: { id },
      data: {
        title,
        url,
        city: {
          connect: { id: cityId },
        },
        author: {
            connect: { id: authorId },
        },
      }
    })
  },

  async deletePicture(parent, { id }, ctx: Context, info) {
    const pictureExists = await ctx.db.exists.Picture({
      id
    })
    if (!pictureExists) {
      throw new Error(`Picture not found`)
    }

    return ctx.db.mutation.deletePicture({ where: { id } })
  },
}
