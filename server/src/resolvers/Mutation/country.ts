import { Context } from '../../utils'

export const country = {
  async createCountry(parent, { name }, ctx: Context, info) {
    return ctx.db.mutation.createCountry(
      {
        data: { name }
      },
      info
    )
  },

  async deleteCountry(parent, { id }, ctx: Context, info) {
    const countryExists = await ctx.db.exists.Country({
      id
    })
    if (!countryExists) {
      throw new Error(`Country not found`)
    }

    return ctx.db.mutation.deleteCountry({ where: { id } })
  },
}
