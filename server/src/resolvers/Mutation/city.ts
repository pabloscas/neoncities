import { Context } from '../../utils'

export const city = {
  async createCity(parent, { name, countryId }, ctx: Context, info) {
    const countryExists = await ctx.db.exists.Country({
        id: countryId
    })
    if (!countryExists) {
        throw new Error(`Country not found`)
    }
    return ctx.db.mutation.createCity(
      {
        data: {
            name,
            country: {
                connect: { id: countryId },
            },
        }
      },
      info
    )
  },

  async deleteCity(parent, { id }, ctx: Context, info) {
    const cityExists = await ctx.db.exists.City({
      id
    })
    if (!cityExists) {
      throw new Error(`City not found`)
    }

    return ctx.db.mutation.deleteCity({ where: { id } })
  },
}
