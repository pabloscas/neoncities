import { Query } from './Query'
import { Subscription } from './Subscription'
import { auth } from './Mutation/auth'
import { post } from './Mutation/post'
import { AuthPayload } from './AuthPayload'
import { country } from './Mutation/country'
import { city } from './Mutation/city'
import { picture } from './Mutation/picture'
import { author } from './Mutation/author'

export default {
  Query,
  Mutation: {
    ...auth,
    ...post,
    ...country,
    ...city,
    ...picture,
    ...author,
  },
  Subscription,
  AuthPayload,
}
