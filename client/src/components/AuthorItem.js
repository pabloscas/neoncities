import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';

import { IoSocialInstagramOutline, IoAndroidLaptop } from 'react-icons/lib/io';

import { SELECTED_AUTHOR_QUERY, SELECT_AUTHOR_MUTATION } from '../queries';

class AuthorItem extends Component {
    displayMiniPictures () {
        return this.props.author.pictures.map((picture, index) => {
            return (
                <div
                    style={{ 'backgroundImage': `url(${picture.url}media/?size=m)` }}
                    key={index}
                    className="author-img author-img--mini"
                />);
        });
    }
    onClick () {
        this.props.mutate({
            variables: {
                authorId: this.props.author.id
            }
        });
    }
    render () {
        const author = this.props.author;
        const selectedId = this.props.data.selectedAuthorId;
        const isActive = selectedId ? (selectedId === author.id ? 'author-item--active' : 'author-item--inactive') : '';
        
        if (author.pictures.length) {
            return (
                <div className={`author-item ${isActive}`}>
                    <div className="author-name">{author.name}</div>
                    <div
                        onClick={this.onClick.bind(this)}
                        className="author-img"
                        style={{ 'backgroundImage': `url(${author.pictures[0].url}media/?size=m)` }}
                    />
                    <div className="author-top-info">
                        <a
                            href={`https://www.instagram.com/${author.instagram}`}
                            className="author-insta"
                            target="_blank"
                        >
                            <IoSocialInstagramOutline />
                        </a>
                        <a href={author.website} className="author-website" target="_blank">
                            <IoAndroidLaptop />
                        </a>
                    </div>
                    <div className="author-tiny-gallery">
                        {this.displayMiniPictures()}
                    </div>
                </div>
            );
        } else {
            return null;
        }
    }
};

export default compose(
    graphql(SELECT_AUTHOR_MUTATION),
    graphql(SELECTED_AUTHOR_QUERY)
)(AuthorItem);