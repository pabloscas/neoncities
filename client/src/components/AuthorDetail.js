import React from 'react';
import { graphql, compose } from 'react-apollo';

import { SELECTED_AUTHOR_QUERY, SELECT_AUTHOR_MUTATION } from '../queries';

const AuthorDetail =  (props) => {
    const veilStatus = props.data.selectedAuthorId ? 'on' : 'off';
    const nullAuthor = {
        variables: {
            authorId: null
        }
    };

    return (
        <div
            className={`veil veil--is-${veilStatus}`}
            onClick={() => props.mutate(nullAuthor)}
        />
    );
};

export default compose(
    graphql(SELECTED_AUTHOR_QUERY),
    graphql(SELECT_AUTHOR_MUTATION)
)(AuthorDetail);