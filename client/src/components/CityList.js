import React from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

const CITIES_QUERY = gql`
    query Cities {
        cities {
            name
            id
            country {
                name
            }
        }
    }
`;


const CityList = (props) => {
    var data = props.data;

    if (data.loading) {
        return <div>Loading...</div>
    } else {
        return data.cities.map(city => <div key={city.id}>{city.name}</div>);
    }
}

export default graphql(CITIES_QUERY)(CityList);