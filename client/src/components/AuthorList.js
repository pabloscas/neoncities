import React from 'react';
import { graphql, compose } from 'react-apollo';

import AuthorItem from './AuthorItem';

import { AUTHORS_QUERY } from '../queries';


class AuthorList extends React.Component {
    displayAuthors () {
        var data = this.props.data;
        
        if (data.loading) {
            return <div className="loading">( Loading... )</div>;
        } else {
            return data.authors.map(author => {
                return <AuthorItem key={author.id} author={author} />;
            });
        }
    }
    render () {
        return (
            <div className="authors-grid">
                {this.displayAuthors()}
            </div>
        );
    }
};

export default graphql(AUTHORS_QUERY)(AuthorList);