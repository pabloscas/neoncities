import React from 'react';
import { withFormik } from 'formik';
import { graphql, compose } from 'react-apollo';
import gql from 'graphql-tag';

const Form = props => {
    const {
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        errors,
        touched,
        isSubmitting
    } = props;

    return (
        <form onSubmit={handleSubmit}>
            <label htmlFor="name" style={{ display: 'block' }}>
                Name
            </label>
            <input
                id="name"
                placeholder="Enter author's name"
                type="text"
                value={values.name}
                onChange={handleChange}
                onBlur={handleBlur}
                className={errors.name && touched.name ? 'text-input error' : 'text-input'}
            />
            {errors.name &&
            touched.name && <div className="input-feedback">{errors.name}</div>}

            <label htmlFor="instagram" style={{ display: 'block' }}>
                Instagram user
            </label>
            <input
                id="instagram"
                placeholder="Enter author's instagram user"
                type="text"
                value={values.instagram}
                onChange={handleChange}
                onBlur={handleBlur}
                className={errors.instagram && touched.instagram ? 'text-input error' : 'text-input'}
            />
            {errors.instagram &&
            touched.instagram && <div className="input-feedback">{errors.instagram}</div>}

            <label htmlFor="website" style={{ display: 'block' }}>
                Website
            </label>
            <input
                id="website"
                placeholder="Enter author's website"
                type="text"
                value={values.website}
                onChange={handleChange}
                onBlur={handleBlur}
                className={errors.website && touched.website ? 'text-input error' : 'text-input'}
            />
            {errors.website &&
            touched.website && <div className="input-feedback">{errors.website}</div>}

            <div>
                <button type="submit" disabled={isSubmitting}>
                    Submit
                </button>
                {isSubmitting && <p>Submitting...</p>}
            </div>
        </form>
    );
};

const EnhancedForm = withFormik({
    mapPropsToValues: () => ({
        name: '',
        instagram: '',
        website: ''
    }),
    handleSubmit: (values, {props, setSubmitting }) => {
        console.log('values', values);
        props.mutate({ variables: values })
            .then((data) => {
                setSubmitting(false);
            });
    },
    displayName: 'CreateAuthorForm'
});

const CreateAuthorMutation = gql`
    mutation($name: String!, $instagram: String, $website: String) {
        createAuthor(name: $name, instagram: $instagram, website: $website) {
            name
            id
        }
    }
`;

export default compose(
    graphql(CreateAuthorMutation),
    EnhancedForm
)(Form);