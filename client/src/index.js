import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import 'typeface-roboto-slab';
import 'roboto-fontface';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();