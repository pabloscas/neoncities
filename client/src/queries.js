import gql from 'graphql-tag';

export const SELECTED_AUTHOR_QUERY = gql`
    query {
        selectedAuthorId @client
    }
`;

export const LOAD_STATUS_QUERY = gql`
    query {
        loadingStatus @client
    }
`;

export const SELECT_AUTHOR_MUTATION = gql`
    mutation updateSelectedAuthorId ($authorId: String) {
        updateSelectedAuthorId (id: $authorId) @client
    }
`;

export const UPDATE_LOADING_STATUS = gql`
    mutation updateLoading ($status: Boolean) {
        updateLoading (status: $status) @client
    }
`;

export const AUTHORS_QUERY = gql`
    query {
        selectedAuthorId @client
        authors {
            name
            id
            instagram
            website
            pictures (first: 3) {
                url
                city {
                    name
                }
            }
        }
    }
`;