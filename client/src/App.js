import React, { Component } from 'react';
import ApolloClient from "apollo-boost";
import { ApolloProvider, graphql } from "react-apollo";

import './App.css';

import AuthorList from './components/AuthorList';
import AuthorDetail from './components/AuthorDetail';
// import CityList from './components/CityList';
// import CreateAuthorForm from './components/CreateAuthorForm';

const graphqlEndpoint = process.env.NODE_ENV === 'production' ? process.env.REACT_APP_GRAPHQL_ENDPOINT : "http://localhost:4000";
console.log(graphqlEndpoint);

const client = new ApolloClient({
  uri: graphqlEndpoint,
  clientState: {
    defaults: {
      selectedAuthorId: null,
      loadingStatus: true
    },
    resolvers: {
      Mutation: {
        updateSelectedAuthorId: (_, { id }, { cache }) => {
          cache.writeData({ data: { selectedAuthorId: id }});
          return null;
        },
        updateLoading: (_, { status }, { cache }) => {
          cache.writeData({ data: { loadingStatus: status }});
          return null;
        }
      }
    }
  }
});

// client.query({
//   query: gql`{
//     cities{
//       name
//     }
//   }`
// })
// .then(result => console.log('result', result));

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <div className="wrapper">
          <main id="main" className="main">
            <h1 className="site-title">Neon Cities</h1>
            <AuthorList />
            <AuthorDetail />
          </main>
          <footer className="footer"></footer>
        </div>
      </ApolloProvider>
    );
  }
}

export default App;
